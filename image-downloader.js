const request = require('request');
const fs = require('fs');
const shell = require('shelljs');

let pathExist = false;
let counter = 0;

function downloadImg(url, {filename = `Image ${counter}`, format = 'jpg', path = './map/original/', callback} = {}) {
    request.get(url, {encoding: 'binary'}, (err, res) => {
        if (!err && res.statusCode == 200) {

            if (!pathExist && !fs.existsSync(path)) {
                shell.mkdir('-p', path);
            }
            pathExist = true;

            console.log(`Saving original image ${url} to ${path}/${filename}.${format}`);
            fs.writeFile(`${path}/${filename}.${format}`, res.body, {encoding: 'binary'}, () => {
                if (callback) {
                    callback(`${filename}.${format}`);
                }
            });

        } else {
            console.log(`failed to download ${url}`);
        }
    });
    counter++;
};

exports = module.exports = downloadImg;
const downloadImg = require('./image-downloader');
const json2csv = require('json2csv');
const fs = require('fs');
const Jimp = require("jimp");
const Stym = require('static-yandex-map');
const BingMapsApi = require('./bing-map-api');

// Extend yandex static-map library to change coords
Stym.prototype.setCoords = function(longitude, latitude) {
    if (Math.abs(longitude) > 180) {
        throw new Error('Static-Yandex-Map: Longitude can be from -180 to +180 degrees!');
    }

    if (Math.abs(latitude) > 90) {
        throw new Error('Static-Yandex-Map: Latitude can be from -90 to +90 degrees!');
    }

    this.parameters.ll = `${longitude},${latitude}`;

};

// Get data 
const PARAMS = require('./params');
const coordinates = PARAMS.COORDS;
const step = PARAMS.STEP;
const path = PARAMS.PATH;
const size = PARAMS.FILE_SIZE;

// Create crop image method 
const cropImage = function(filename) {
    Jimp.read(path.originalImg + filename, function(err, img) {
        if (err) throw err;

        img.crop(0, 0, size.crop[0], size.crop[1])
            .quality(100)
            .write(path.croppedImg + filename);
    });
};
const csvFields = ['topLeftPos', 'bottomRightPos'];
const processImage = function({x, y} = {}, filename) {
    cropImage(filename);
    console.log(`${filename} cropped`);

    const csv = json2csv({
        data: {
            'topLeftPos': `${x},${y}`,
            'bottomRightPos': `${x + step.horWithPadding},${y + step.verWithPadding}`
        },
        fields: csvFields
    });

    fs.writeFile(path.croppedImg + filename.replace(PARAMS.TILEFORMAT, 'csv'), csv, function(err) {
        if (err) throw err;
        console.log(`CSV data for ${filename} saved`);
    });
};

let service = null;
if (PARAMS.SERVICE === 'Bing') {
    service = BingMapsApi;
} else if (PARAMS.SERVICE === 'Yandex') {
    service = Stym;
}

if (service == null) {
    throw Error('No such serivce');
}

const s = new service(coordinates.start.longitude, coordinates.start.latitude);
// Start from top left corner 
s.setType('satellite')
    .setSize(size.download[0], size.download[1])
    .setZ(17);

// Init Counters
let xCounter = 0;
let yCounter = 0;
let overallCounter = 0;
let interval = PARAMS.INTERVAL * 1000;

// Cycle through all tiles
for (let y = coordinates.start.latitude; y >= coordinates.end.latitude; y -= step.ver) {
    for (let x = coordinates.start.longitude; x <= coordinates.end.longitude; x += step.hor) {
        setTimeout(function(xCounter, yCounter, x, y) {
            s.setCoords(x, y);
            downloadImg(s.getUrl(), {
                path: path.originalImg,
                format: PARAMS.TILEFORMAT,
                filename: `${PARAMS.FILENAME}_${xCounter}_${yCounter}`,
                callback: function(img, name) {
                    processImage({x, y}, img, name);
                }
            });
        }, interval * overallCounter, xCounter, yCounter, x, y);
        yCounter++;
        overallCounter++;
    }
    xCounter++;
    overallCounter++;
    yCounter = 0;
}


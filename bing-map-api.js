// https://dev.virtualearth.net/REST/V1/Imagery/Map/Aerial/55.9%2C48.85/19?mapSize=830,830&format=png&key=Ah12kIOAYzmW3ONiCcYEImWIp2xL8pwMXEvlOjhzjLRhjBBOCgBygJe4PHOY_aKU
var BingMapsApi = function(longitude, latitude) {
    if (Math.abs(longitude) > 180) {
        throw new Error('Static-Bing-Map: Longitude can be from -180 to +180 degrees!');
    }

    if (Math.abs(latitude) > 90) {
        throw new Error('Static-Bing-Map: Latitude can be from -90 to +90 degrees!');
    }

    this.url = 'https://dev.virtualearth.net/REST/V1/Imagery/Map';
    this.params = {
        'll': latitude + ',' + longitude,
        'l': 'map',
        'mapType': 'Aerial',
        'lang': 'ru-RU',
        'size': '650,650',
        'format': 'png',
        'key': 'Ah12kIOAYzmW3ONiCcYEImWIp2xL8pwMXEvlOjhzjLRhjBBOCgBygJe4PHOY_aKU'
    };
    if (process.env.yandexMapApiKey) {
        this.parameters.key = process.env.yandexMapApiKey;
    }
    this.points = [];
    return this;
};

BingMapsApi.prototype.setLocation = function(longitude, latitude) {
    if (Math.abs(longitude) > 180) {
        throw new Error('Static-Bing-Map: Longitude can be from -180 to +180 degrees!');
    }

    if (Math.abs(latitude) > 90) {
        throw new Error('Static-Bing-Map: Latitude can be from -90 to +90 degrees!');
    }
    this.params.ll = latitude + ',' + longitude;
    return this;
};

BingMapsApi.prototype.setCoords = BingMapsApi.prototype.setLocation;

BingMapsApi.prototype.setType = function(mapType) {
    switch (mapType) {
        case 'satellite':
            mapType = 'Aerial';
            break;
        default:
            throw Error('No such map type or not implementer yet');
    }
    this.params.mapType = mapType;
    return this;
};

BingMapsApi.prototype.setFromat = function(format) {
    // TODO: add availible types
    this.params.format = format;
    return this;
};

BingMapsApi.prototype.setSize = function(width, height) {
    if (width > 900) {
        throw new Error('900px is max width');
    }

    if (height > 834) {
        throw new Error('834px is max width');
    }

    this.params.size = `${width},${height}`;
    return this;
};

BingMapsApi.prototype.setKey = function(key) {
    this.params.key = key;
    return this;
};

BingMapsApi.prototype.setZ = function(zoom) {
    this.params.zoom = zoom;
    return this;
};

BingMapsApi.prototype.getUrl = function() {
    return `${this.url}/${this.params.mapType}/${this.params.ll}/19?mapSize=${this.params.size}&format=${this.params.format}&key=${this.params.key}`;
};

BingMapsApi.prototype.toString = function() {
    return this.getUrl();
};

exports = module.exports = BingMapsApi;
# README #

Script to download closest map tiles from yandex static api 

### What is this repository for? ###
Basic script to download closest tiles from yandex static api, crop watermark and store csv information about location
* Version: 0.0.1

#### Environment: ####
* Node JS >=7.0
* NPM

#### Install: ####  
* Install nodejs
* Clone and open project folder
* Type `npm i` or `npm install` in console to install application additional packages
* Type `npm start` to start application 

#### Configuration ####
To update configuration change constants in `params.js` file
```javascript
    COORDS: {
        start: {x: 34.295023, y: 53.304815}, // Top left coords to download map from
        end: {x: 34.305023, y: 53.294815} // Bottom right coords to download
    },
    STEP: {
        hor: HOR_DELTA, // Horizontal delta in coord between tiles
        ver: VERT_DELTA // Vertical delta between tiles
    },
    PATH: {
        originalImg: PATH_TO_ORIGINAL_IMG, // Path to save original image
        croppedImg: PATH_TO_CROPPED_IMG // Path to save cropped image
    },
    FILE_SIZE: {
        download: [450, 450], // 450px download image file size (max 650x450)
        crop: [400, 400]
    },
    FILENAME: 'MapTile' // Filename to save
```

### Re ###
* aronov.aleks@yandex.ru
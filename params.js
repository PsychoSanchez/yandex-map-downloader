// Init angles delta
// Yandex delta
// const HOR_DELTA = 0.0043;
// const VERT_DELTA = 0.00256;
// const HOR_PADDING = 0.00038;
// const VERT_PADDING = 0.000252;
const HOR_DELTA = 0.00174;
const VERT_DELTA = 0.0012;
const VERT_PADDING = 0.00018;
const HOR_PADDING = 0.00025;
const DEFAULT_PATH = './map/';
const PATH_TO_ORIGINAL_IMG = `${DEFAULT_PATH}/original/`;
const PATH_TO_CROPPED_IMG = DEFAULT_PATH;

exports = module.exports = {
    SERVICE: 'Bing',
    COORDS: {
        start: {longitude: 48.85, latitude: 55.9},
        end: {longitude: 49.092, latitude: 55.794}
    },
    STEP: {
        hor: HOR_DELTA - HOR_PADDING,
        ver: VERT_DELTA - VERT_PADDING,
        horWithPadding: HOR_DELTA,
        verWithPadding: VERT_DELTA
    },
    PATH: {
        originalImg: PATH_TO_ORIGINAL_IMG,
        croppedImg: PATH_TO_CROPPED_IMG
    },
    FILE_SIZE: {
        download: [650, 650],
        crop: [600, 600]
        // download: [450, 450],
        // crop: [400, 400]
    },
    FILENAME: 'MapTile',
    TILEFORMAT: 'png',
    REWRITE_TILES: true,
    INTERVAL: .5
}